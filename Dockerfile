FROM registry.gitlab.com/natural-solutions/geonature/zones-humides/atlas:latest as builder

WORKDIR /app

ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1
EXPOSE 3000

COPY ./data/public /app/public
COPY ./data/config.yml /app/data/config.yml

RUN npm run publish

FROM nginx:alpine

COPY --from=builder /app/out /usr/share/nginx/html
